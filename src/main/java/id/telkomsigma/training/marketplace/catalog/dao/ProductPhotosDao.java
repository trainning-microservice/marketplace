package id.telkomsigma.training.marketplace.catalog.dao;

import id.telkomsigma.training.marketplace.catalog.entity.Product;
import id.telkomsigma.training.marketplace.catalog.entity.ProductPhotos;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductPhotosDao extends PagingAndSortingRepository<ProductPhotos, String> {
    public Iterable<ProductPhotos> findByProduct(Product product);
}
