package id.telkomsigma.training.marketplace.catalog.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import id.telkomsigma.training.marketplace.catalog.entity.Product;

public interface ProductDao extends PagingAndSortingRepository<Product, String> {
	
}
